--	Módulo Divisor de Frecuencia
--	Copyright (C) 2017  Oropeza Vilchis Luis Alberto <vilchissfi@gmail.com>
--
--	This program is free software: you can redistribute it and/or modify
--	it under the terms of the GNU General Public License as published by
--	the Free Software Foundation, either version 3 of the License, or
--	(at your option) any later version.
--
--	This program is distributed in the hope that it will be useful,
--	but WITHOUT ANY WARRANTY; without even the implied warranty of
--	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--	GNU General Public License for more details.
--
--	You should have received a copy of the GNU General Public License
--	along with this program.  If not, see <http://www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.all;

entity divisor_frecuencia is
	generic (
		f_in: integer:= 50000000; -- Frecuencia del reloj de entrada
		f_out: integer:= 1	-- Frecuencia de salida
	);
	port (
		reloj: in std_logic;
		s: buffer std_logic
	);
end entity;

architecture archDF of divisor_frecuencia is
	signal f: integer:= f_in/f_out;
begin
	process (reloj)
		variable pulsos: integer := 0;
	begin
		if rising_edge(reloj) then
			pulsos := pulsos + 1;
			if pulsos = f then
				pulsos := 0;
				s <= not(s);
			end if;
		end if;
	end process;
end architecture archDF;