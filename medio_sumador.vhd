--	Medio sumador
--	Copyright (C) 2017  Oropeza Vilchis Luis Alberto <vilchissfi@gmail.com>
--
--	This program is free software: you can redistribute it and/or modify
--	it under the terms of the GNU General Public License as published by
--	the Free Software Foundation, either version 3 of the License, or
--	(at your option) any later version.
--
--	This program is distributed in the hope that it will be useful,
--	but WITHOUT ANY WARRANTY; without even the implied warranty of
--	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--	GNU General Public License for more details.
--
--	You should have received a copy of the GNU General Public License
--	along with this program.  If not, see <http://www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.all;


entity medio_sumador is
	port (
		a, b: in std_logic; -- Bits de entrada
		s, c: out std_logic -- Bit de salida y carry
	);
end entity;

architecture archMS of medio_sumador is
begin
	s <= a xor b;
	c <= a and b;
end architecture archMS;
