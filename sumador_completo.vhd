--	Sumador completo, implementado con dos medios sumadores.
--	Copyright (C) 2017  Oropeza Vilchis Luis Alberto <vilchissfi@gmail.com>
--
--	This program is free software: you can redistribute it and/or modify
--	it under the terms of the GNU General Public License as published by
--	the Free Software Foundation, either version 3 of the License, or
--	(at your option) any later version.
--
--	This program is distributed in the hope that it will be useful,
--	but WITHOUT ANY WARRANTY; without even the implied warranty of
--	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--	GNU General Public License for more details.
--
--	You should have received a copy of the GNU General Public License
--	along with this program.  If not, see <http://www.gnu.org/licenses/>.


library ieee;
use ieee.std_logic_1164.all;

entity SC is
	port (
		a, b, ce: in std_logic; -- Entradas, carry entrada
		s, cs: out std_logic --Salida y carry de salida
	);
end entity;

architecture archSC of SC is
	signal s1, c1, c2: std_logic;

begin
	MS1: entity work.medio_sumador -- Primer medio sumador
		port map (
			a => a,
			b => b,
			s => s1,
			c => c1
		);
	MS2: entity work.medio_sumador -- Segundo medio sumador
		port map (
			a => ce,
			b => s1,
			s => s,
			c => c2
		);
	cs <= c1 or c2; -- Carry final
end architecture archSC;